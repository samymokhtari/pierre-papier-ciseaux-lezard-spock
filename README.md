**Pierre Papier Ciseaux Lézard Spock**
==

L'objectif de ce projet universitaire est de réaliser une interface sur Android permettant:
* l'inscription
* la connexion
* un tutoriel expliquant le fonctionnement du jeu
* jouer au jeu contre un bot d'une difficulté à 5 niveau (très facile, facile, normal, difficile, invincible)
* visualiser le classement des différents joueurs sur le jeu

Équipe:
* Léo TAVERNIER
* Samy MOKHTARI

*Groupe 2 - DUT2 S4* 