package com.samy.rock_paper_cisors_lezard_spock;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.net.PasswordAuthentication;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "base.sqlite3" ;
    private static final int DATABASE_VERSION = 1;
    private static final char[] hexArray="0123456789IUT2020".toCharArray();

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlCreateUsersTable = "create table if not exists Users ("
                +       "id INTEGER PRIMARY KEY autoincrement,"
                +       "firstname TEXT NOT NULL,"
                +       "lastname TEXT NOT NULL,"
                +       "email TEXT NOT NULL,"
                +       "login TEXT NOT NULL,"
                +       "age INTEGER NOT NULL,"
                +       "gender INTEGER NOT NULL,"
                +       "password TEXT NOT NULL,"
                +       "score INTEGER NOT NULL,"
                +       "UNIQUE(login)"
                +       ")";

        db.beginTransaction();
        try {
            db.execSQL(sqlCreateUsersTable);
            Log.i("DATABASE","onCreate {USERS TABLE} INVOKED");
            db.setTransactionSuccessful();
        } catch(Exception ex){
            throw ex;
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DATABASE", "onUpgrade Invoked");
    }

    public static String generateHash(String p_password) throws NoSuchAlgorithmException {
        MessageDigest msgDigest = MessageDigest.getInstance("MD5");
        msgDigest.reset();
        byte [] hash=msgDigest.digest(p_password.getBytes());
        return byteToStringHex(hash);
    }

    private static String byteToStringHex(byte[] p_bytes) {
        char[] hexChars = new char[p_bytes.length * 2];
        for (int v_index = 0; v_index < p_bytes.length; v_index++){
            int v_temp = p_bytes[v_index] & 0xFF ;
            hexChars[v_index * 2]=hexArray[ v_temp >>> 4];
            hexChars[v_index * 2 + 1]=hexArray[ v_temp & 0x0F];
        }
        return new String(hexChars);
    }

    public void createUser(String p_firstname, String p_lastname, String p_email,String p_login, String p_password, short p_age, short p_gender) throws NoSuchAlgorithmException {
        String v_hashedPassword = generateHash(p_password);

        String sqlCommand = "insert into Users (firstname,lastname,email,login,password,age,gender,score)" +
                " VALUES ('"+p_firstname+"', '"+p_lastname+"', '"+p_email+"', '"+p_login+"', '"+v_hashedPassword+"', '"+p_gender+"', "+0+" ) ";

        this.getWritableDatabase().execSQL(sqlCommand);
        Log.i("DATABASE","User created");
    }

    // Récupère le classement dans une limite de 10 personnes
    public List<User> fetchRanking(){
        List<User> users = new ArrayList<>();
        Cursor cursor = this.getReadableDatabase().rawQuery( "SELECT * FROM Users order by score desc limit 10", null);
        cursor.moveToFirst();
        while(! cursor.isAfterLast()) {
            //instanciation d'un users et ajout dans la liste d'users
            try{
                users.add ( new User (
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        Boolean.valueOf(String.valueOf(cursor.getInt(6))),
                        cursor.getString(7),
                        cursor.getInt(8)
                ));
            }catch(Exception e){
                Log.i("error","Erreur dans le constructeur de l'utilisateur");
            }
            cursor.moveToNext();
        }
        return users;
    }

    // Récupère le champion depuis la base de données
    public User getChampion(){
        return fetchRanking().get(0);
    }


}
