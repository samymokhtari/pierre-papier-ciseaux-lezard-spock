package com.samy.rock_paper_cisors_lezard_spock;

import android.util.Log;
import android.widget.ArrayAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IA {
    /* Attributs */
    byte _difficulty;

    public byte getDifficulty() {
        return _difficulty;
    }

    public void setDifficulty(byte p_difficulty) {
        if(p_difficulty>=1 && p_difficulty<=5){
            //This mathematic formul because difficulty can be 1,2,3,4 or 5 and so we have 2,4,6,8,10
            this._difficulty = (byte) (2*p_difficulty);
        }else{
            throw new IllegalArgumentException("Difficulty need to be > 1 and < 5");
        }
    }

    /* Méthodes */
    public IA(byte p_difficulty){
        setDifficulty(p_difficulty);
    }

    // This method take a random between 1 and _difficulty -> so we have 1/_difficulty chance of win as a user.
    public Choice play(Choice p_valuePlayedUser){
        Random v_rand = new Random();
        Choice v_choicePlayedByIA;

        int v_randomInteger=1+v_rand.nextInt(_difficulty);
        if(v_randomInteger==1){
            v_choicePlayedByIA=p_valuePlayedUser.getRandomChoice(true);
        }else{
            v_choicePlayedByIA=p_valuePlayedUser.getRandomChoice(false);
        }

        return v_choicePlayedByIA;
    }
}
