package com.samy.rock_paper_cisors_lezard_spock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

public class MenuActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    byte _difficulty;
    User _user;
    FirebaseManager _firebaseManager;
    ScrollView _arrayRanking;
    Spinner _spinnerDifficulty;
    TextView _textRanking;
    TextView _textRankUser;
    TextView _txtDifficulty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        _difficulty = 1;
         _user = (User) getIntent().getExtras().getSerializable("user");
        _firebaseManager=new FirebaseManager();
        _arrayRanking = (ScrollView) findViewById(R.id.scrollview_ranking);
        _textRanking = (TextView) findViewById(R.id.txt_ranking);
        _txtDifficulty = (TextView) findViewById(R.id.txt_difficulty);
        _textRankUser = (TextView) findViewById(R.id.txt_rank);
        _firebaseManager.getRankOf(_user.getId(), _textRankUser);
        _spinnerDifficulty= findViewById(R.id.spn_difficultLvl);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.difficultiy,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        _spinnerDifficulty.setAdapter(adapter);
        _spinnerDifficulty.setOnItemSelectedListener(this);
    }

    // Start the game if the parameters are okay
    public void startGame(View view){
        Intent myIntent= new Intent(this, PlayActivity.class);
        Bundle myBundle= new Bundle();
        myBundle.putSerializable("user", _user);
        myBundle.putByte("difficulty", _difficulty);

        myIntent.putExtras(myBundle);
        startActivity(myIntent);
    }

    public void refreshRanking(View view){
        showRanking();
    }

    // show ranking in the scrollview
    public void showRanking(){
        _textRanking.setText("");
        final StringBuilder textRanking= new StringBuilder();
        _firebaseManager.readData(new FirebaseCallback() {
            @Override
            public void onCallback(List<User> users) {
                Collections.sort(users);
                for(User user : users){
                    textRanking.append(user.getLogin()+"\t\r"+" →  SCORE : "+user.getScore()+'\n');
                }
                _textRanking.setText(textRanking.toString());
            }
        });
    }

    // Show the activity rules
    public void showRules(View view){
        Intent myIntent= new Intent(this, RulesActivity.class);
        startActivity(myIntent);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        _difficulty=(byte) (position+1);
        _txtDifficulty.setText("Niveau de difficulté: "+parent.getItemAtPosition(position).toString());
        _txtDifficulty.setTextColor(Color.rgb(255,200-(_difficulty*40),87));
       // Toast.makeText(parent.getContext(),"Niveau de difficulté : "+parent.getItemAtPosition(position).toString(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
