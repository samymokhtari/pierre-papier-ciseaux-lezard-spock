package com.samy.rock_paper_cisors_lezard_spock;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {
    int _round;
    int _roundWon;
    byte _difficulty;
    User _user;
    FirebaseManager _firebaseManager;
    Button _btnPlay;
    ImageView _imgRock;
    ImageView _imgPaper;
    ImageView _imgScissors;
    ImageView _imgLezard;
    ImageView _imgSpock;
    TextView _txtRound;
    TextView _txtRoundWon;
    ArrayList<ImageView> _listImage;
    TextView _valuePlayedUser;
    TextView _valuePlayedIA;
    IA _ia;
    Choice _userChoice;
    Choice _iaChoice;

    Choice _rock;
    Choice _paper;
    Choice _scissors;
    Choice _lezard;
    Choice _spock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        _user = (User) getIntent().getExtras().getSerializable("user");
        _difficulty=getIntent().getExtras().getByte("difficulty");
        _firebaseManager=new FirebaseManager();
        _valuePlayedUser=(TextView) findViewById(R.id.txt_valuePlayedUser);
        _btnPlay=(Button) findViewById(R.id.btn_jouer);
        _valuePlayedIA=(TextView) findViewById(R.id.txt_valuePlayedIA);
        _txtRound=findViewById(R.id.txt_round);
        _txtRoundWon =findViewById(R.id.txt_roundWon);
        _round=1;
        _txtRound.setText("Round "+_round);
        _imgRock = (ImageView) findViewById(R.id.img_rock);
        _imgPaper = (ImageView) findViewById(R.id.img_paper);
        _imgScissors = (ImageView) findViewById(R.id.img_scissors);
        _imgLezard = (ImageView) findViewById(R.id.img_lezard);
        _imgSpock = (ImageView) findViewById(R.id.img_spock);
        _listImage=new ArrayList<ImageView>();
        _listImage.add(_imgRock);
        _listImage.add(_imgPaper);
        _listImage.add(_imgScissors);
        _listImage.add(_imgLezard);
        _listImage.add(_imgSpock);

        _imgRock.setOnClickListener(this);
        _imgPaper.setOnClickListener(this);
        _imgScissors.setOnClickListener(this);
        _imgLezard.setOnClickListener(this);
        _imgSpock.setOnClickListener(this);

        _ia = new IA(_difficulty);

        // Possibilité également de générer tout ça via la firebase
        _rock=new Choice("rock");
        _paper=new Choice("paper");
        _scissors=new Choice("scissors");
        _lezard=new Choice("lezard");
        _spock=new Choice("spock");

        _rock.fillArrayVictory(new Choice[]{_scissors, _lezard});
        _rock.fillArrayDefeat(new Choice[]{_paper, _spock,_rock});

        _paper.fillArrayVictory(new Choice[]{_rock, _spock});
        _paper.fillArrayDefeat(new Choice[]{_scissors, _lezard,_paper});

        _scissors.fillArrayVictory(new Choice[]{_paper, _lezard});
        _scissors.fillArrayDefeat(new Choice[]{_rock, _spock,_scissors});

        _lezard.fillArrayVictory(new Choice[]{_paper, _spock});
        _lezard.fillArrayDefeat(new Choice[]{_rock, _scissors,_lezard});

        _spock.fillArrayVictory(new Choice[]{_rock, _scissors});
        _spock.fillArrayDefeat(new Choice[]{_paper, _lezard,_spock});
    }

    public void launchRound(View view) throws InterruptedException {
        try{
            _iaChoice=_ia.play(_userChoice);
            _valuePlayedUser.setText(_userChoice.getName().toUpperCase());
            _valuePlayedIA.setText(_iaChoice.getName().toUpperCase());

        }catch (Exception ex){
            _valuePlayedUser.setText("Veuillez sélectionner un choix");
            ex.printStackTrace();
            return;
        }
        _round++;
        if(_userChoice.getVictory().contains(_iaChoice)){
            _roundWon++;

            _txtRoundWon.setText("Manche gagnées: "+ _roundWon);
            if(_roundWon == 3){
                _firebaseManager.updateUserScore(_user,_difficulty*10);
                Toast.makeText(this, "VICTOIRE !", Toast.LENGTH_LONG).show();
                endGame();
                return;
            }

        }
        else if (_userChoice==_iaChoice){
            _round--;
            Toast.makeText(this, "ÉGALITÉ ! Recommencez", Toast.LENGTH_SHORT).show();
        }
        if(_round==6) {
            Toast.makeText(this, "DÉFAITE...", Toast.LENGTH_LONG).show();
            endGame();
            return;
        }

        _txtRound.setText("Round "+_round);
    }

    @Override
    public void onClick(View v) {
        final int RGB = android.graphics.Color.argb(60, 0, 0, 255);
        switch(v.getId()) {
            case (R.id.img_rock):

                setColorImageSelected(RGB,_imgRock);
                _userChoice=_rock;
                break;
            case (R.id.img_paper):
                setColorImageSelected(RGB,_imgPaper);
                _userChoice=_paper;
                break;
            case (R.id.img_scissors):
                setColorImageSelected(RGB,_imgScissors);
                _userChoice=_scissors;
                break;
            case (R.id.img_lezard):
                setColorImageSelected(RGB,_imgLezard);
                _userChoice=_lezard;
                break;
            case (R.id.img_spock):
                setColorImageSelected(RGB,_imgSpock);
                _userChoice=_spock;
                break;
            default:
        }
    }

    private void endGame() throws InterruptedException {
        _btnPlay.setEnabled(false);
        this.finish();
    }

    private void setColorImageSelected(int color, ImageView imageSelected){
        for(ImageView img : _listImage){
            img.setBackgroundColor(Color.TRANSPARENT);
        }
        imageSelected.setBackgroundColor(color);
    }
}
