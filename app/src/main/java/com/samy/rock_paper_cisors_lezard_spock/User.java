package com.samy.rock_paper_cisors_lezard_spock;

import android.util.Log;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User implements Comparable<User>, Serializable {

    /* Attributs */
    private long _id;
    private String _firstname;
    private String _lastname;
    private String _email;
    private String _login;
    private String _password;
    private int _age;
    private boolean _gender;
    private int _score;
    private final String REGEX_EMAIL = "^(.+)@(.+)$";

    /* Méthodes */
    public User(long p_id, String p_firstname, String p_lastname, String p_email, String p_login, boolean  p_gender,String p_password, int p_score) throws Exception {
        this.setId(p_id);
        this.setFirstname(p_firstname);
        this.setLastname(p_lastname);
        this.setEmail(p_email);
        this.setLogin(p_login);
        this.setPassword(p_password);
        this.setGender(p_gender);
        this.setScore(p_score);
    }

    /* GETTER AND SETTERS */
    public long getId() {
        return _id;
    }

    public void setId(long p_id) {
        this._id = p_id;
    }

    public String getFirstname() {
        return _firstname;
    }

    public void setFirstname(String p_firstname) { //CONDITION: Length > 2
        if(p_firstname.length()>=2){
            this._firstname = p_firstname;
        } else
            throw new IllegalArgumentException("La longueur du prénom doit être supérieur à 2");
    }

    public String getLastname() {
        return _lastname;
    }

    public void setLastname(String p_lastname) {//CONDITION: Length > 2
        if(p_lastname.length()>=2){
            this._lastname = p_lastname;
        } else
            throw new IllegalArgumentException("La longueur du nom doit être supérieur à 2");

    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String p_email) { //CONDITION: Email valid format: ***@***.***
        Pattern pattern = Pattern.compile(REGEX_EMAIL);
        Matcher matcher = pattern.matcher(p_email);
        if(matcher.matches()!=false){
            this._email = p_email;
        } else {
            throw new IllegalArgumentException("Format de l'email incorrect");
        }

    }

    public String getLogin() {
        return _login;
    }

    public void setLogin(String p_login) {
        this._login = p_login;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String p_password) {
        if(p_password.length()>5){
            this._password = p_password;
        }
    }

    public boolean getGender() {
        return _gender;
    }

    public void setGender(boolean p_gender) {
            this._gender = p_gender;
    }

    public int getScore() {
        return _score;
    }

    public void setScore(int p_score) {
        if(p_score>=0){
            this._score = p_score;
        } else {
            throw new IllegalArgumentException("Le score ne peut pas être négatif");
        }
    }

    @Override
    public String toString() {
        return getFirstname()+" "+getLastname()+" | Identifiant: "+getLogin()+" | Score: "+getScore();
    }

    // Overload of the compareTo method of the interface 'Comparable'  which compare one user to another according to his score
    @Override
    public int compareTo(User o) {
        return Integer.valueOf((int) (this._score + o.getScore()));
    }
}
