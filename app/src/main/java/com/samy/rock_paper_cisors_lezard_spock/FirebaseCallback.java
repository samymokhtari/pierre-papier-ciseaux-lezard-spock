package com.samy.rock_paper_cisors_lezard_spock;

import java.util.List;

public interface FirebaseCallback {
    void onCallback(List<User> users);
}
