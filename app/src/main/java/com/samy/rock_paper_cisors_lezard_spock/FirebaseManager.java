package com.samy.rock_paper_cisors_lezard_spock;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FirebaseManager {

    /* Attributs */
    private DatabaseReference _databaseReference;
    private long _maxId;
    private List<User> _users;

    /* Méthodes */

    // Get the id maximum in the database
    public long getMaxId() {
        _databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    _maxId=dataSnapshot.getChildrenCount();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("ERROR",databaseError.getMessage());
            }
        });
        return _maxId;
    }

    public FirebaseManager(){
        _databaseReference= FirebaseDatabase.getInstance().getReference().child("User");
        _maxId=getMaxId();
        _users = new ArrayList<>();
    }

    // Create a new user and register him in the firebase
    public boolean createUser(User p_user){
        boolean v_result=false;
        try{
            _databaseReference.child(String.valueOf(getMaxId()+1)).setValue(p_user);
            v_result=true;
        } catch (Exception ex){
            Log.i("FIREBASE",ex.toString());
            v_result=false;
        } finally {
            return v_result;
        }
    }

    // Callback function which fetch all user data in a list of User
    public void readData(final FirebaseCallback firebaseCallback){
        _users.clear();
        _databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (int index = 1; index <= getMaxId(); index++) {
                    try {
                        _users.add(new User(
                                Long.valueOf(index),
                                dataSnapshot.child(String.valueOf(index)).child("firstname").getValue().toString(),
                                dataSnapshot.child(String.valueOf(index)).child("lastname").getValue().toString(),
                                dataSnapshot.child(String.valueOf(index)).child("email").getValue().toString(),
                                dataSnapshot.child(String.valueOf(index)).child("login").getValue().toString(),
                                Boolean.getBoolean(dataSnapshot.child(String.valueOf(index)).child("gender").getValue().toString()),
                                dataSnapshot.child(String.valueOf(index)).child("password").getValue().toString(),
                                Integer.valueOf(dataSnapshot.child(String.valueOf(index)).child("score").getValue().toString())
                        ));
                    } catch (Exception e) {
                        Log.i("LOGIN", "Erreur dans le constructeur de l'utilisateur: ");
                        e.printStackTrace();
                    }
                }
                firebaseCallback.onCallback(_users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("LOGIN",databaseError.getMessage());
            }
        });
    }

    //Update an user's score
    public void updateUserScore(User p_user, final int p_score){
        p_user.setScore(p_user.getScore()+p_score);
        _databaseReference.child(String.valueOf(p_user.getId())).setValue(p_user);
    }

    public void getRankOf(final long p_idUser, final TextView p_textView){
        readData(new FirebaseCallback() {
            @Override
            public void onCallback(List<User> users) {
                Collections.sort(users);
                for(int index=0 ; index<users.size();index++){
                    if(users.get(index).getId()==p_idUser){
                        if(index==0){
                            p_textView.setTextColor(Color.rgb(255, 171, 0));
                            p_textView.setText("Vous êtes classé "+(index+1)+ "er");
                        }else{
                            p_textView.setText("Vous êtes classé "+(index+1)+ "ème");
                        }

                    }
                }
            }
        });
    }

}
