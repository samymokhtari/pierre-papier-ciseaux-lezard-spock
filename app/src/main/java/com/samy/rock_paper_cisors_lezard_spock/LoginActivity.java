package com.samy.rock_paper_cisors_lezard_spock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private SQLiteDatabase _database;
    private TextView _error;
    private TextView _login;
    private TextView _password;
    private DatabaseManager _databaseManager;
    private FirebaseManager _firebaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        _error = findViewById(R.id.textView_error);
        _error.setText("");
        _login = findViewById(R.id.plainText_name);
        _password = findViewById(R.id.plainText_password);
        _databaseManager = new DatabaseManager(this);
        _database = _databaseManager.getWritableDatabase();
        _firebaseManager=new FirebaseManager();

        _databaseManager.close();
    }

    // Verify a attempt of a connection and allow it if the user was found
    public void verifyConnection(View view) throws NoSuchAlgorithmException {
        final String v_login = _login.getText().toString().trim();
        final String v_password = _password.getText().toString().trim();
        final String hashedPassword=DatabaseManager.generateHash(v_password);

        if(v_login.isEmpty() || v_password.isEmpty()){
            _error.setText("L'identifiant et le mot de passe sont obligatoires");
            return;
        }

        _firebaseManager.readData(new FirebaseCallback() {
            @Override
            public void onCallback(List<User> users) {
                for (User user : users) {
                    if (user.getLogin().equals(v_login) && user.getPassword().equals(hashedPassword)) {
                        _error.setTextColor(Color.GREEN);
                        _error.setText("Connexion réussie !");
                        launchActivityMenu(user.getId(),user);
                    }
                }
            }
        });
        _error.setTextColor(Color.RED);
        _error.setText("L'identifiant et/ou le mot de passe sont incorrects");
    }

    // Start the menu activity by specify in a bundle the id of the user
    public void launchActivityMenu(long p_idUser,User p_user){
        Intent myIntent= new Intent(this, MenuActivity.class);

        Bundle myBundle= new Bundle();
        myBundle.putSerializable("user",p_user);

        myIntent.putExtras(myBundle);

        try{
            startActivity(myIntent);
        } catch (Exception e){
            _error.setText("L'identifiant et/ou le mot de passe sont incorrects");
        }
    }

    // Start the register activity and open the form associate
    public void launchActivityRegister(View view){
        Intent myIntent= new Intent(this, RegistrationActivity.class);
        startActivity(myIntent);
    }
}
