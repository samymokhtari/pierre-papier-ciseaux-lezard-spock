package com.samy.rock_paper_cisors_lezard_spock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.util.LocaleData;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.time.LocalDate;

public class RegistrationActivity extends AppCompatActivity {

    private FirebaseManager _firebaseManager;
    private TextView _email;
    private TextView _login;
    private TextView _password;
    private TextView _firstname;
    private TextView _lastname;
    private TextView _error;
    private RadioGroup _radioGroup;
    private RadioButton _radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        _firebaseManager=new FirebaseManager();
        _radioGroup = findViewById(R.id.rgr_sex);
        _email=findViewById(R.id.plainText_email);
        _login=findViewById(R.id.plainText_login);
        _password=findViewById(R.id.plainText_password);
        _lastname=findViewById(R.id.plainText_lastname);
        _firstname=findViewById(R.id.plainText_firstname);
        _error = findViewById(R.id.txtView_error);
    }

    public void validateRegistration(View view){
        try{
            _firebaseManager.createUser( new User (
                    _firebaseManager.getMaxId()+1,
                    _firstname.getText().toString(),
                    _lastname.getText().toString(),
                    _email.getText().toString(),
                    _login.getText().toString(),
                    Boolean.valueOf(_radioButton.getTag().toString()),
                    DatabaseManager.generateHash(_password.getText().toString()),
                    0)
            );

            Toast.makeText(this,"Utilisateur créer avec succès",Toast.LENGTH_LONG).show();
            finish();
        } catch (Exception ex){
            ex.printStackTrace();
            _error.setText("Des informations sont manquantes ou incorrects");
            Toast.makeText(this,"Echec de création de l'utilisateur",Toast.LENGTH_LONG).show();
        }
    }

    public void checkButton(View view){
        int radioButtonId = _radioGroup.getCheckedRadioButtonId();
        _radioButton=findViewById(radioButtonId);
    }
}
