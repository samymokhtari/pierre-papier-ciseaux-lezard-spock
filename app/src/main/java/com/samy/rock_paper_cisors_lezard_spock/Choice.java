package com.samy.rock_paper_cisors_lezard_spock;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class Choice {
    // Attributes
    private String _name;
    private ArrayList<Choice> _victory;
    private ArrayList<Choice> _defeat;



    // Methods
    public Choice(String p_name) {
        setName(p_name);
        _victory=new ArrayList<Choice>();
        _defeat=new ArrayList<Choice>();
    }

    // if the parameters are true we take a "victory choice" to return to make the IA won, if not we make the IA lost the round
    public Choice getRandomChoice(boolean p_issue){
        Random v_random = new Random();
        int v_randomInteger;
        if(p_issue==true){
            v_randomInteger = v_random.nextInt(_victory.size());
            return _victory.get(v_randomInteger);
        } else {
            v_randomInteger = v_random.nextInt(_defeat.size());
            return _defeat.get(v_randomInteger);
        }
    }

    //fill the victory array with the good choices (array in parameters for the futur maintenance of the application)
    public void fillArrayVictory(Choice p_choices[]){
        for(Choice choice : p_choices){
            _victory.add(choice);
        }
    }
    //same as above
    public void fillArrayDefeat(Choice p_choices[]){
        for(Choice choice : p_choices){
            _defeat.add(choice);
        }
    }

    public String getName() {
        return _name;
    }

    public Choice setName(String _name) {
        this._name = _name;
        return this;
    }

    public ArrayList<Choice> getVictory() {
        return _victory;
    }

    public ArrayList<Choice> getDefeat() {
        return _defeat;
    }
}
